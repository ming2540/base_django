from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="MY API",
        default_version='v1',
        description="",
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

admin.site.site_header = 'MYAPP'
admin.site.index_title = 'MYAPP Administration'
admin.site.site_title = 'MYAPP Administration'

urlpatterns = [
    path('api/admin/', admin.site.urls),
    path('api/common/', include('apps.common.urls')),
    path('api/users/', include('apps.users.urls')),
]

# Add docs URL on debug only
if settings.DEBUG:
    from django.conf.urls.static import static

    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns.append(
        path('api/docs/', schema_view.with_ui('redoc', cache_timeout=0))
    )
