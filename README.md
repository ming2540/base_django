POS backend Django project. This project is using for generate and maintain database schema and generate PDF report.

# Installation
1. Install python 3.7+
2. Run command ```pip install -r requirements.txt```
3. Create database with postgres ```CREATE DATABASE <DB_NAME>;```
4. Create environment file ```.env```
```
DEBUG=<boolean>
```
5. Migrate database ```python manage.py migrate```
6. Load all fixtures file ```python manage.py loaddata fixtures/*.json```

# Notes
- Create admin user
```
python manage.py createsuperuser
```
- Create fixture file
```
python manage.py dumpdata app.model_name --indent 4 > fixtures/file_name.json
```
- Run Test With pytest
https://pytest-django.readthedocs.io/en/latest/

# Celery
- Install RabbitMQ (MAC)
    1. `brew install rabbitmq`
    2. Add `export PATH=${PATH}:/usr/local/sbin` to `~/.zshrc`
    3. Manually start with command `rabbitmq-server`
- Install Celery
    1. ```pip install -r requirements.txt```
    2. ```python manage.py migrate```
- Run process
    1. Start RabbitMQ `rabbitmq-server` (MUST BE STARTED BEFORE CELERY)
    2. Start Celery `celery -A pos_backend worker -Q celery`
    - NOTE: Use workon before running celery command
    - NOTE2: Re-start celery when edit the file `celery.py` or `tasks.py`
- Example
    1. Please see sample code at apps/core/tasks/create_address_task.py
    2. Run sample code with this script
    ```
        import celery
        celery.current_app.send_task('test-create-address', ['<ROAD_NAME>'])
    ```
    3. Look at the "Task restults" in Django Admin page and Check Thai Address Model