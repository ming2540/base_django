﻿from django.urls import path
from apps.users import views

urlpatterns = [
    path('api-token-auth/', views.CustomAuthToken.as_view(), name='api-token-auth'),
]