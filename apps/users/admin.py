from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model
from apps.users import models


@admin.register(get_user_model())
class CustomUserAdmin(UserAdmin):
    list_display = ['username', 'first_name', 'last_name', 'is_active']
    search_fields = ['username', 'first_name', 'last_name']
