from django.contrib.auth import authenticate, login, logout, get_user_model
from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.users.serializers import UserSerializer

User = get_user_model()


class CustomAuthToken(APIView):
    """
    Custom class to create a token.
    This method do not create new user
    """
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        username = request.data.get('username', None)
        if username is None or username == '':
            raise serializers.ValidationError({'detail': 'Please enter a username'})

        user = User.objects.filter(username=username).first()
        if not user:
            raise serializers.ValidationError({'detail': 'อุปกรณ์ยังไม่ได้ลงทะเบียน'})
        else:
            login(request, user)
            token, created = Token.objects.get_or_create(user=user)
            return Response({
                'token': token.key,
                'user': UserSerializer(user).data
            })
