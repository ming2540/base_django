import random
import string
import uuid
from datetime import timedelta

from constance import config
from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.gis.db import models
from django.utils import timezone
from django_currentuser.db.models import CurrentUserField
from rest_framework.authentication import TokenAuthentication

from frameworks.models import BaseModel, BaseModelQuerySet


class BearerTokenAuthentication(TokenAuthentication):
    keyword = 'Bearer'


class BaseUserModelManager(UserManager):
    def actives(self):
        return self.get_queryset().filter(is_active=True)

    def inactives(self):
        return self.get_queryset().filter(is_active=False)


class CustomUser(AbstractUser):
    """
    Custom User which extends AbstractUser
    Add add UUID
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True, help_text='วัน-เวลาสร้าง')
    updated = models.DateTimeField(auto_now=True, help_text='วัน-เวลาแก้ไขล่าสุด')
    created_by = CurrentUserField(related_name='+')
    updated_by = CurrentUserField(related_name='+', on_update=True)
    password = models.CharField(max_length=128, null=True, blank=True, verbose_name='password')

    objects = BaseUserModelManager()

    @property
    def display_name(self):
        if self.first_name and self.last_name:
            return '%s %s' % (self.first_name, self.last_name)
        else:
            return self.username

    def delete(self, *args, **kwargs):  # Prevent delete of actual object
        self.is_active = False
        self.save()
