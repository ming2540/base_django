from constance import config
from django.conf import settings
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, views
from rest_framework.serializers import ValidationError
from rest_framework.response import Response
from apps.common import models, serializers


class ProvinceViewSet(viewsets.ReadOnlyModelViewSet):
    """จังหวัด"""
    queryset = models.Province.objects.all()
    serializer_class = serializers.ProvinceSerializer
    pagination_class = None


class DistrictViewSet(viewsets.ReadOnlyModelViewSet):
    """อำเภอ"""
    queryset = models.District.objects.all()
    serializer_class = serializers.DistrictSerializer

    province_id = openapi.Parameter(
        'province_id', openapi.IN_PATH, description="ID of the province used for filter", type=openapi.TYPE_INTEGER)
    response = openapi.Response('List of filtered district', serializers.DistrictSerializer(many=True))

    @swagger_auto_schema(manual_parameters=[province_id], responses={200: response})
    def list(self, request, province_id):
        queryset = models.District.objects.filter(province__id=province_id)
        serializer = serializers.DistrictSerializer(queryset, many=True)
        return Response(serializer.data)


class SubDistrictViewSet(viewsets.ReadOnlyModelViewSet):
    """ตำบล"""
    queryset = models.SubDistrict.objects.all()
    serializer_class = serializers.SubDistrictSerializer

    district_id = openapi.Parameter(
        'district_id', openapi.IN_PATH, description="ID of the province used for filter", type=openapi.TYPE_INTEGER)
    response = openapi.Response('List of filtered district', serializers.SubDistrictSerializer(many=True))

    @swagger_auto_schema(manual_parameters=[district_id], responses={200: response})
    def list(self, request, district_id):
        queryset = models.SubDistrict.objects.filter(district__id=district_id)
        serializer = serializers.SubDistrictSerializer(queryset, many=True)
        return Response(serializer.data)
