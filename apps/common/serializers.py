from rest_framework import serializers
from apps.common import models


class ProvinceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Province
        fields = '__all__'


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.District
        fields = '__all__'


class SubDistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SubDistrict
        fields = '__all__'
