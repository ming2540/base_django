from django.urls import path, include
from rest_framework.routers import DefaultRouter
from apps.common import views

router = DefaultRouter()
router.register('province', views.ProvinceViewSet)
router.register('district/(?P<province_id>[^/.]+)', views.DistrictViewSet)
router.register('sub-district/(?P<district_id>[^/.]+)', views.SubDistrictViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
