from django.db import models
from frameworks.models import BaseModel


class Region(BaseModel):
    """ภูมิภาค"""
    name_th = models.CharField(max_length=500)
    name_en = models.CharField(max_length=500)

    def __str__(self):
        return self.name_th

    class Meta:
        verbose_name = 'ภาค'
        verbose_name_plural = 'ภาค'
        ordering = ['id']

    @staticmethod
    def autocomplete_search_fields():
        return 'name_th',


class Province(BaseModel):
    """จังหวัด"""
    name_th = models.CharField(max_length=500)
    name_en = models.CharField(max_length=500)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)

    def __str__(self):
        return self.name_th

    class Meta:
        verbose_name = 'จังหวัด'
        verbose_name_plural = 'จังหวัด'
        ordering = ['id']

    @staticmethod
    def autocomplete_search_fields():
        return 'name_th',


class District(BaseModel):
    """เขต/อำเภอ"""
    name_th = models.CharField(max_length=500)
    name_en = models.CharField(max_length=500)
    province = models.ForeignKey(Province, on_delete=models.CASCADE)

    def __str__(self):
        return '%s (%s)' % (self.name_th, self.province.name_th)

    class Meta:
        verbose_name = 'เขต/อำเภอ'
        verbose_name_plural = 'เขต/อำเภอ'
        ordering = ['id']

    @staticmethod
    def autocomplete_search_fields():
        return 'name_th',


class SubDistrict(BaseModel):
    """แขวง/ตำบล"""
    name_th = models.CharField(max_length=500)
    name_en = models.CharField(max_length=500)
    zip_code = models.IntegerField()
    district = models.ForeignKey(District, on_delete=models.CASCADE)

    def __str__(self):
        return '%s (%s) (%s) - %s' % (
            self.name_th,
            self.district.name_th,
            self.district.province.name_th,
            self.zip_code
        )

    class Meta:
        verbose_name = 'แขวง/ตำบล'
        verbose_name_plural = 'แขวง/ตำบล'
        ordering = ['id']

    @staticmethod
    def autocomplete_search_fields():
        return 'name_th',
