from celery import current_app

from constance import config
from django.core.mail import get_connection
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string


class SendEmailTask(current_app.Task):
    name = "send-email-task"

    def run(
        self, subject, context, recipient_list,
        attach_file_path=None, attach_mime_type=None
    ):
        connection = get_connection(
            host=config.EMAIL_HOST,
            port=config.EMAIL_PORT,
            username=config.EMAIL_USERNAME,
            password=config.EMAIL_PASSWORD,
            use_tls=config.USE_TLS,
            use_ssl=config.USE_SSL,
        )
        email_msg = EmailMultiAlternatives(
            subject,
            context,
            config.EMAIL_USERNAME,
            recipient_list,
            connection=connection,
        )
        if attach_file_path and attach_mime_type:
            email_msg.attach_file(attach_file_path, attach_mime_type)
        email_msg.send()


current_app.register_task(SendEmailTask())
