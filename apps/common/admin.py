from django.contrib import admin
from apps.common import models


@admin.register(models.Province)
class ProvinceAdmin(admin.ModelAdmin):
    readonly_fields = models.Province.base_attrs()
    search_fields = ['name_th', 'name_en']

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(models.District)
class DistrictAdmin(admin.ModelAdmin):
    readonly_fields = models.District.base_attrs()
    autocomplete_fields = ['province']
    search_fields = [
        'name_th', 'name_en',
        'province__name_th', 'province__name_en',
    ]

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}


@admin.register(models.SubDistrict)
class SubDistrictAdmin(admin.ModelAdmin):
    readonly_fields = models.SubDistrict.base_attrs()
    autocomplete_fields = ['district']
    search_fields = [
        'name_th', 'name_en',
        'district__name_th', 'district__name_en',
        'district__province__name_th', 'district__province__name_en',
    ]

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}
